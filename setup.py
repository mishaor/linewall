from setuptools import setup, find_packages


readme = open('README.md', 'r')

setup(
    name="linewall",
    version="1.0",
    packages=find_packages(),
    entrypoints={
        'console_scripts': [
            'linewall = linewall.__main__'
        ]
    },
    install_requires="pillow ==6,>=6.0.0",
    author="Michael Orishich",
    author_email="mishaor@ukr.net",
    description="A simple wallpaper generator.",
    long_description=readme,
    long_description_content_type='text/markdown',
    keywords="lines wallpaper generator",
    project_urls={
        "Source Code": "https://gitlab.com/mishaor/linewall",
    },
    classifiers=[
        'License :: OSI Approved :: MIT License'
    ]
)
