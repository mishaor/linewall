from PIL import Image, ImageDraw
from random import choice


class Generator:
    def __init__(self, config):
        # read the config from a config dict
        self.background = config['background']
        self.colors = config['colors']
        self.line_count = int(config['lineCount'])

        self.width = int(config['width'])
        self.height = int(config['height'])

    def generate(self):
        # create a new image
        image = Image.new('RGBA', (self.width, self.height), self.background)

        # calculate the line thickness based on line count and width
        line_thickness = int(self.width / self.line_count)

        draw = ImageDraw.Draw(image)

        # draw n lines with random colors over the image
        for i in range(0, self.line_count):
            # nice wallpaper generator lies in this comment
            # draw.line([((line_thickness * i) - line_thickness, 0), (line_thickness * i, self.height)],
            #          fill=choice(self.colors), width=line_thickness)
            draw.line([((line_thickness * i) + (line_thickness / 2), 0), ((line_thickness * i) + (line_thickness / 2), self.height)],
                      fill=choice(self.colors), width=line_thickness)

        # return the image
        return image
