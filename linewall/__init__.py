from .generator import Generator

from argparse import ArgumentParser
from json import load
from sys import stdout


def cli():
    parser = ArgumentParser()
    parser.add_argument('-o', '--output', type=str, required=True)
    parser.add_argument('config', type=str)
    args = parser.parse_args()

    # parse config
    config = load(open(args.config, 'r'))

    # create a Generator
    gen = Generator(config)

    # generate the image
    image = gen.generate()

    image.save(args.output)
